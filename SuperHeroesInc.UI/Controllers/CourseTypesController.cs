﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuperHeroesInc.DATA;

namespace SuperHeroesInc.UI.Controllers
{
    public class CourseTypesController : Controller
    {
        private SuperHeroesIncVilHeroEntities db = new SuperHeroesIncVilHeroEntities();

        // GET: CourseTypes
        public ActionResult Index()
        {
            return View(db.CourseTypes1.ToList());
        }

        // GET: CourseTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseTypes courseTypes = db.CourseTypes1.Find(id);
            if (courseTypes == null)
            {
                return HttpNotFound();
            }
            return View(courseTypes);
        }

        // GET: CourseTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CourseTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseTypeID,Name,Description")] CourseTypes courseTypes)
        {
            if (ModelState.IsValid)
            {
                db.CourseTypes1.Add(courseTypes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(courseTypes);
        }

        // GET: CourseTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseTypes courseTypes = db.CourseTypes1.Find(id);
            if (courseTypes == null)
            {
                return HttpNotFound();
            }
            return View(courseTypes);
        }

        // POST: CourseTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseTypeID,Name,Description")] CourseTypes courseTypes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(courseTypes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(courseTypes);
        }

        // GET: CourseTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseTypes courseTypes = db.CourseTypes1.Find(id);
            if (courseTypes == null)
            {
                return HttpNotFound();
            }
            return View(courseTypes);
        }

        // POST: CourseTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CourseTypes courseTypes = db.CourseTypes1.Find(id);
            db.CourseTypes1.Remove(courseTypes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
