﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SuperHeroesInc.DATA;

namespace SuperHeroesInc.UI.Controllers
{
    public class CoursesController : Controller
    {
        private SuperHeroesIncVilHeroEntities db = new SuperHeroesIncVilHeroEntities();

        // GET: Courses
        public ActionResult Index()
        {
            var courses1 = db.Courses1.Include(c => c.CourseType);
            return View(courses1.ToList());
        }

        // GET: Courses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Courses courses = db.Courses1.Find(id);
            if (courses == null)
            {
                return HttpNotFound();
            }
            return View(courses);
        }

        // GET: Courses/Create
        public ActionResult Create()
        {
            ViewBag.CourseTypeID = new SelectList(db.CourseTypes1, "CourseTypeID", "Name");
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseID,Name,Description,CourseTypeID")] Courses courses)
        {
            if (ModelState.IsValid)
            {
                db.Courses1.Add(courses);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseTypeID = new SelectList(db.CourseTypes1, "CourseTypeID", "Name", courses.CourseTypeID);
            return View(courses);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Courses courses = db.Courses1.Find(id);
            if (courses == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseTypeID = new SelectList(db.CourseTypes1, "CourseTypeID", "Name", courses.CourseTypeID);
            return View(courses);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseID,Name,Description,CourseTypeID")] Courses courses)
        {
            if (ModelState.IsValid)
            {
                db.Entry(courses).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseTypeID = new SelectList(db.CourseTypes1, "CourseTypeID", "Name", courses.CourseTypeID);
            return View(courses);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Courses courses = db.Courses1.Find(id);
            if (courses == null)
            {
                return HttpNotFound();
            }
            return View(courses);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Courses courses = db.Courses1.Find(id);
            db.Courses1.Remove(courses);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
